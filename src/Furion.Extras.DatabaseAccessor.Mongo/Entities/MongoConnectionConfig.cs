﻿namespace MongoDB
{
    /// <summary>
    /// MongoDB数据库连接配置
    /// </summary>
    public class MongoConnectionConfig
    {
        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString { get; set; }
        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DatabaseName { get; set; }
    }
}
